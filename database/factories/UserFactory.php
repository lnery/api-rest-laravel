<?php

use Faker\Generator as Faker;

$factory->define(App\User::class, function (Faker $faker) {
    return [
        'username' => $faker->username,
        'first_name' => $faker->first_name,
        'last_name' => $faker->last_name,
        'password' => $faker->password
    ];
});