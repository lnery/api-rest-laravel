<?php

use Illuminate\database\Seeder;

class UserTableSeeder extends Seeder
{

    public function run()
    {

        factory(App\User::class)->create([
            'username' => 'lbnery',
            'first_name' => 'Lucas',
            'last_name' => 'Nery',
            'password' => '1234'
        ]);

    }

}