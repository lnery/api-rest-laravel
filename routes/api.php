<?php

use Illuminate\Http\Request;

Route::get("users", "UserController@getAll");
Route::get("users/{user}", "UserController@getUser");
Route::post("users", "UserController@createUser");
Route::patch("users/{user}", "UserController@updateUser");
Route::delete("users/{user}", "UserController@destroyUser");