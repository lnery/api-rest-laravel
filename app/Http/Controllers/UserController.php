<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Contracts\Validation\Validator;

class UserController extends Controller
{
    
    public function getAll()
    {
         
        return User::all();

    }

    public function getUser(User $User)
    {

        return $User;

    }

    public function createUser(Request $request)
    {

        $request->validate([
            'username' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'password' => 'required'
        ]);

        $User = User::create([
            'username' => $request->input('username'),
            'first_name' => $request->input('first_name'),
            'last_name' => $request->input('last_name'),
            'password' => $request->input('password')
        ]);

        return $User;

    }

    public function updateUser(Request $request, User $User)
    {

        $request->validate([
            'username' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'password' => 'required'
        ]);

        $User->name = $request->input('name');

        $User->save();

        return $User;

    }

    public function destroyUser(User $User)
    {

        $User->delete();

        return responde()->json(['success'=>true]);

    }

}